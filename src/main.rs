mod day_3;
//mod day_5;
mod day_6;
//mod day_7;
//mod day_8;
mod day_10;

fn task(day: i32, part: i32) {
  let mut res = String::new();

  match day {
    3 => res = day_3::main(part),
    6 => res = day_6::main(part),
    //7 => res = day_7::part1(),
    //8 => res = day_8::main(part),
    10 => res = day_10::main(part),
    _ => res = "Not a valid value".to_string(),
  }

  println!("{res}");
}

fn main() {
  task(10,1)
}
