use std::fs;

#[derive(Eq, Hash, PartialEq, Debug)]
struct Shift {
    start: i32, 
    end: i32,
}
#[derive(Eq, Hash, PartialEq, Debug)]
struct PairAssigment {
    first: Shift,
    second: Shift,
}

type SectionsPerElf = Vec<PairAssigment>;

fn split_line_into_pairs(line: &str) -> PairAssigment {
    let split_pair: Vec<&str> = line.split(",").collect();
    let shift1: Vec<&str> = split_pair[0].split("-").collect();
    let shift2: Vec<&str> = split_pair[1].split("-").collect();
    return PairAssigment{
            first: Shift{
                start: shift1[0].parse::<i32>().unwrap(), 
                end: shift1[1].parse::<i32>().unwrap()}, 
            second: Shift{
                start: shift2[0].parse::<i32>().unwrap(), 
                end: shift2[1].parse::<i32>().unwrap(),}
        };
}

fn check_if_wholly_overlap(pair_assigment: PairAssigment) -> bool {
    if pair_assigment.first.start >= pair_assigment.second.start 
        && pair_assigment.first.end <= pair_assigment.second.end {
        return true;
    }
    else if pair_assigment.second.start >= pair_assigment.first.start 
        && pair_assigment.second.end <= pair_assigment.first.end {
        return true;
    }
    false
}

fn if_any_overlap(pair: PairAssigment) -> bool {
    (pair.second.start <= pair.first.start && pair.second.start >= pair.first.end)
        || (pair.first.start <= pair.second.start && pair.first.start >= pair.second.end)
        || (pair.second.end >= pair.first.end && pair.second.start <= pair.first.end)
        || (pair.first.end >= pair.second.end && pair.first.start  <= pair.second.end) 
}

pub fn part1() {
    let input = fs::read_to_string("../input/input_day4.txt").unwrap();
    let mut amount_overlap = 0;
    let mut all_pair_assigment = SectionsPerElf::new();
    let lines: Vec<&str> = input.lines().collect();
    for l in lines {
        all_pair_assigment.push(split_line_into_pairs(l));
    }

    for p in all_pair_assigment {
        if check_if_wholly_overlap(p) {
            amount_overlap = amount_overlap + 1; 
        }
    }
    
    println!("{amount_overlap}");
}

pub fn part2() {
    let input = fs::read_to_string("../input/input_day4.txt").unwrap();
    let mut amount_overlap = 0;
    let mut all_pair_assigment = SectionsPerElf::new();
    let lines: Vec<&str> = input.lines().collect();
    for l in lines {
        all_pair_assigment.push(split_line_into_pairs(l));
    }

    for p in all_pair_assigment {
        if if_any_overlap(p) {
            amount_overlap = amount_overlap + 1; 
        }
    }
    
    println!("{amount_overlap}");
}
