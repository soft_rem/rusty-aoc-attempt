use std::fs;
use std::collections::HashSet;

fn parse_input(input: &str, size_limit: usize) -> String {
    let chars: Vec<char> = input.chars().collect();
    let length_limit = input.len() - (size_limit - 1);
    let mut unique_chars: HashSet<char> = Default::default();
    let mut selected_chars: Vec<char> = Vec::new();
    for (idx, c) in (&chars[0 ..=length_limit]).iter().enumerate() {
        unique_chars.clear();
        for (jdx, c) in (&chars[idx..=idx+size_limit-1]).iter().enumerate() {
            unique_chars.insert(*c);
        }
        if unique_chars.len() == size_limit {
            return (idx + size_limit).to_string();
        }
    }

    panic!("No unique sequential char pattern found.");
}

pub fn main(part: i32) -> String {
    let input = fs::read_to_string("../input/input_day6.txt").unwrap();

    let mut res = String::new();

    match part {
        1 => res = parse_input(input.as_str(), 4),
        2 => res = parse_input(input.as_str(), 14),
        _ => panic!("Not a valid value"),
    }

    res
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part1() {
        let res1 = parse_input("bvwbjplbgvbhsrlpgdmjqwftvncz", 4);
        assert_eq!(res1, "5");

        let res2 = parse_input("nppdvjthqldpwncqszvftbrmjlhg", 4);
        assert_eq!(res2, "6");
    }

    #[test]
    fn test_part2() {
        let res1 = parse_input("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 14);
        assert_eq!(res1, "19");
    }
}
