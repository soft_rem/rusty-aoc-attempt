use std::fs;

pub fn part1() {
    let input = fs::read_to_string("../input/input_day2.txt").unwrap();

    let split_guide: Vec<&str> = input.split("\n").collect();
    let mut score = 0;

    for round in split_guide {
        let mut res = 0;
        match round {
            "A X" => res = 1 + 3, 
            "A Y" => res = 2 + 6,
            "A Z" => res = 3,
            "B X" => res = 1,
            "B Y" => res = 2 + 3,
            "B Z" => res = 3 + 6,
            "C X" => res = 1 + 6,
            "C Y" => res = 2,
            "C Z" => res = 3 + 3,
            _ => res = 0
        }

        score = score + res;
    }

    println!("{score}");
}

pub fn part2() {
     let input = fs::read_to_string("../input/input_day2.txt").unwrap();

    let split_guide: Vec<&str> = input.split("\n").collect();
    let mut score = 0;

    for round in split_guide {
        let mut res = 0;
        match round {
            "A X" => res = 3, 
            "A Y" => res = 1 + 3,
            "A Z" => res = 2 + 6,
            "B X" => res = 1,
            "B Y" => res = 2 + 3,
            "B Z" => res = 3 + 6,
            "C X" => res = 2,
            "C Y" => res = 3 + 3,
            "C Z" => res = 1 + 6,
            _ => res = 0
        }

        score = score + res;
    }

    println!("{score}");

}
