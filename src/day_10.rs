use std::fs;

enum Cmd {
    noop, // one circle, nothing else
    addx(i32), // two circle, then add value
}

// if remainder of modulus 40 is 20, it is the cycle where we save the value
fn is_required_cycle(cycle: &i32) -> bool {
    let rem = cycle % 40;
    rem == 20
}

fn parse_cmd(cmd: &str) -> Cmd {
    let cmd_vec: Vec<&str> = cmd.split_whitespace().collect();
    match cmd_vec[0] {
        "noop" => Cmd::noop,
        "addx" => Cmd::addx(cmd_vec[1].parse::<i32>().unwrap()),
        _ => panic!("Not a command"),
    }
}

// Part 1: Find signal strengh at the 20th, 60th, 100th, 140th, 180th and 220th cycle 
// Return sum of the signal strength
// signal strength = addx_val * cycle
pub fn parse_input(input: String) -> String {
    let cmds: Vec<&str> = input.lines().collect();
    let mut saved_signal_strength: Vec<i32> = Vec::<i32>::new();
    let mut cycle = 1;
    let mut reg_val = 1;
    for cmd in cmds {
        let cmd_type = parse_cmd(cmd);
        match cmd_type {
            Cmd::noop => cycle += 1,
            Cmd::addx(val) => {
                cycle += 1;
                if is_required_cycle(&cycle) {
                    saved_signal_strength.push(cycle * reg_val);
                }
                cycle += 1;
                reg_val += val;
            },
            _ => panic!("Not a command"),
        }

        if is_required_cycle(&cycle) {
            saved_signal_strength.push(cycle * reg_val);
        }
    }

    return saved_signal_strength.iter().sum::<i32>().to_string();
}

pub fn main(part: i32) -> String {
    let input = fs::read_to_string("../input/input_day10.txt").unwrap();

    match part {
        1 => parse_input(input),
        _ => panic!("Not a part in day 10"),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn part1() {
        let test_input = fs::read_to_string("../input/input_day10_test.txt").unwrap();

        assert_eq!(parse_input(test_input), "13140");
    }
}
