use std::fs;

pub fn part1() {
    let input= fs::read_to_string("../input/input_day1.txt").expect("Can't read file");
    
    let split_by_elf: Vec<&str> = input.split("\n\n").collect();
    let amount_elves = split_by_elf.len();
    let mut calc_per_elf: Vec<&i32> = Vec::<&i32>::new();
    let mut max = 0;
    for elf in split_by_elf {
        let mut amount_calc = 0;
        let calc_per_elf_str: Vec<&str> = elf.split("\n").collect();
        for str in calc_per_elf_str {
            amount_calc = amount_calc + str.parse::<i32>().unwrap_or(0);
        }
        if amount_calc > max {
            max = amount_calc;
        }
    }

    println!("{max}");
    //println!("{input}");
}

pub fn part2() {
    let input = fs::read_to_string("../input/input_day1.txt").unwrap();

    let split_by_elf: Vec<&str> = input.split("\n\n").collect();
    let amount_elves = split_by_elf.len();
    let mut calc_per_elf: Vec<&i32> = Vec::<&i32>::new();
    let mut max = 0;
    let mut max_three = [0, 0, 0];
    for elf in split_by_elf {
        println!("{elf} \n\n\n");
        let mut amount_calc = 0;
        let calc_per_elf_str: Vec<&str> = elf.split("\n").collect();
        for str in calc_per_elf_str {
            amount_calc = amount_calc + str.parse::<i32>().unwrap_or(0);
        }
        if amount_calc > max_three[0] {
            let temp_max = max_three[0];
            max_three[0] = amount_calc;
            max_three[2] = max_three[1];
            max_three[1] = temp_max;
        }
        else if amount_calc > max_three[1] {
            max_three[2] = max_three[1];
            max_three[1] = amount_calc;
        }
        else if amount_calc > max_three[2] {
            max_three[2] = amount_calc;
        }
    }

    for i in max_three {
        max = max + i;
    }

    println!("{max}");
}
